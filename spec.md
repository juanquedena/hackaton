# Tech U! Practitioner Backend API API documentation version v1.0.0
http://api.techu.practitioner.backend.com/{environment}/{version}

---

## /products

### /products

* **get**: List all products
* **post**: Create a new product

### /products/{product_id}

* **get**: Retrieve a exists product
* **put**: Replace a exists product
* **patch**: Update a property of exists product

### /products/{product_id}/users

* **get**: List all users from product
* **put**: Add a user to the product

### /products/{product_id}/users/{user_id}

* **delete**: Delete a exists user from product

## /users

### /users

* **get**: List all users
* **post**: Create a new user

### /users/{user_id}

* **delete**: Delete a exists user

