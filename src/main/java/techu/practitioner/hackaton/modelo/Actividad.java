package techu.practitioner.hackaton.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Actividad {

    private String id;
    private String descripcion;
    private String id_perfil;
    private String id_tipo_aplicacion;
    private String id_criterio;
    private String complejidad;
    private int cantidad;

    @JsonIgnore
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(String id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getId_tipo_aplicacion() {
        return id_tipo_aplicacion;
    }

    public void setId_tipo_aplicacion(String id_tipo_aplicacion) {
        this.id_tipo_aplicacion = id_tipo_aplicacion;
    }

    public String getId_criterio() {
        return id_criterio;
    }

    public void setId_criterio(String id_criterio) {
        this.id_criterio = id_criterio;
    }

    public String getComplejidad() {
        return complejidad;
    }

    public void setComplejidad(String complejidad) {
        this.complejidad = complejidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
