package techu.practitioner.hackaton.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class Proyecto {

    private String id;
    private String id_sda;
    private String descripcion;

    @JsonIgnore
    private List<Requerimiento> requerimientos;

    @JsonIgnore
    public String getId() {
        return id;
    }

    @JsonIgnore
    public void setId(String id) {
        this.id = id;
    }

    public String getId_sda() {
        return id_sda;
    }

    public void setId_sda(String id_sda) {
        this.id_sda = id_sda;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Requerimiento> getRequerimientos() {
        if(requerimientos == null)
            requerimientos = new ArrayList<>();
        return requerimientos;
    }

    public void setRequerimientos(List<Requerimiento> requerimientos) {
        this.requerimientos = requerimientos;
    }
}
