package techu.practitioner.hackaton.modelo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

public class Perfil {

	@Id
    private String id;
    private String descripcion;
    private List<TipoAplicacion> tipoAplicaciones;

    @JsonIgnore
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @JsonIgnore
    public List<TipoAplicacion> getTipoAplicaciones() {
        return tipoAplicaciones;
    }

    public void setTipoAplicaciones(List<TipoAplicacion> tipoAplicaciones) {
        this.tipoAplicaciones = tipoAplicaciones;
    }
}
