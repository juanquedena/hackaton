package techu.practitioner.hackaton.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Criterio {

	@JsonIgnore
    private String id;
    private String descripcion;
    private String abreviatura;
    private Complejidad complejidad;

    @JsonIgnore
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public Complejidad getComplejidad() {
        return complejidad;
    }

    public void setComplejidad(Complejidad complejidad) {
        this.complejidad = complejidad;
    }
}
