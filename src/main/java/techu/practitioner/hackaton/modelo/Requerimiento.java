package techu.practitioner.hackaton.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Requerimiento {

    private String id;
    private String descripcion;
    private List<Actividad> actividades;

    @JsonIgnore
    public String getId() {
        return id;
    }

    @JsonIgnore
    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Actividad> getActividades() {
        if(actividades == null)
            actividades = new ArrayList<>();
        return actividades;
    }

    @JsonIgnore
    public void setActividades(List<Actividad> actividades) {
        this.actividades = actividades;
    }
}
