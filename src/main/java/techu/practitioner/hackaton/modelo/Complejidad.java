package techu.practitioner.hackaton.modelo;

public class Complejidad {

    private double bajo;
    private double medio;
    private double alto;

    public double getBajo() {
        return bajo;
    }

    public void setBajo(double bajo) {
        this.bajo = bajo;
    }

    public double getMedio() {
        return medio;
    }

    public void setMedio(double medio) {
        this.medio = medio;
    }

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }
}
