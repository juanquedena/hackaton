package techu.practitioner.hackaton.modelo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TipoAplicacion {
	
    private String id;
    private String descripcion;

    @JsonIgnore
    private List<Criterio> criterios;

    @JsonIgnore
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Criterio> getCriterios() {
        return criterios;
    }

    public void setCriterios(List<Criterio> criterios) {
        this.criterios = criterios;
    }
}
