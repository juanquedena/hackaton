package techu.practitioner.hackaton.servicio.repositorio;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import techu.practitioner.hackaton.modelo.Perfil;

@Repository
public interface RepositorioPerfil extends MongoRepository<Perfil, String>{
	

}
