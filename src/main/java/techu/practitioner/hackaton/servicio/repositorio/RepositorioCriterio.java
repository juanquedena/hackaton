package techu.practitioner.hackaton.servicio.repositorio;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import techu.practitioner.hackaton.modelo.Criterio;

@Repository
public interface RepositorioCriterio extends MongoRepository<Criterio, String>{
	
	Optional<Criterio> findByAbreviatura(String abreviatura);
}
