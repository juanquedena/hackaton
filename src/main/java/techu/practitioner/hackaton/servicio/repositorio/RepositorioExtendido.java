package techu.practitioner.hackaton.servicio.repositorio;

import java.util.List;

import techu.practitioner.hackaton.modelo.Criterio;

public interface RepositorioExtendido {
		
	public List<Criterio> obtenerCriterios(String idPerfil, String idTp);
	
	public List<Criterio> obtenerCriteriosId(String idPerfil, String idTp, String idCri);

	public void agregarCriterio(String idPerfil, String idTp, Criterio cri);

	public void actualizarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio, Criterio c);
	
	public void eliminarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio);

}
