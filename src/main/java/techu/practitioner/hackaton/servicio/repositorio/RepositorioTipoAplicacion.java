package techu.practitioner.hackaton.servicio.repositorio;

import org.springframework.data.mongodb.repository.MongoRepository;
import techu.practitioner.hackaton.modelo.TipoAplicacion;

public interface RepositorioTipoAplicacion  extends MongoRepository<TipoAplicacion, String> {
}
