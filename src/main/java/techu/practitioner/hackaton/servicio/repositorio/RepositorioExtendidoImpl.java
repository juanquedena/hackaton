package techu.practitioner.hackaton.servicio.repositorio;


import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.replaceRoot;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import techu.practitioner.hackaton.modelo.Criterio;
import techu.practitioner.hackaton.modelo.Perfil;
@Repository
public class RepositorioExtendidoImpl implements RepositorioExtendido{

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Override
	public List<Criterio> obtenerCriterios(String idPerfil, String idTp) {
		
		return this.mongoTemplate.aggregate(newAggregation(
				
              match(Criteria.where("_id").is(idPerfil).and("tipoAplicaciones._id").is(idTp))

              , unwind("$tipoAplicaciones")

              , match(Criteria.where("_id").is(idPerfil).and("tipoAplicaciones._id").is(idTp))
              
              , unwind("$tipoAplicaciones.criterios")

              , replaceRoot("$tipoAplicaciones.criterios")

      ), "perfil", Criterio.class).getMappedResults();
		
	}

	@Override
	public List<Criterio> obtenerCriteriosId(String idPerfil, String idTp, String idCri) {
		// TODO Auto-generated method stub
		return this.mongoTemplate.aggregate(newAggregation(
				
	              match(Criteria.where("_id").is(idPerfil).and("tipoAplicaciones._id").is(idTp))

	              , unwind("$tipoAplicaciones")

	              , match(Criteria.where("_id").is(idPerfil).and("tipoAplicaciones._id").is(idTp))
	              
	              , unwind("$tipoAplicaciones.criterios")
	              
	              , match(Criteria.where("tipoAplicaciones.criterios._id").is(idCri))
	              
	              , replaceRoot("$tipoAplicaciones.criterios")

	      ), "perfil", Criterio.class).getMappedResults();
	}

	@Override
	public void agregarCriterio(String idPerfil, String idTp, Criterio cri) {
		
		Query query = new Query(Criteria.where("_id").is(idPerfil));
		Update update = new Update().addToSet("tipoAplicaciones.$[tipoAp].criterios.", cri)
		        .filterArray(Criteria.where("tipoAp._id").is(idTp));
		mongoTemplate.findAndModify(query, update, Perfil.class);
		
	}

	@Override
	public void actualizarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio, Criterio c) {
		Query query = new Query(Criteria.where("_id").is(idPerfil));
		Update update = new Update().set("tipoAplicaciones.$[tipoAp].criterios.$[criter].", c)
		        .filterArray(Criteria.where("tipoAp._id").is(idTipoAplicacion))
		        .filterArray(Criteria.where("criter._id").is(idCriterio));
		mongoTemplate.findAndModify(query, update, Perfil.class);
		
	}

	@Override
	public void eliminarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio) {
		Query query = Query.query(Criteria
                .where("_id").is(idPerfil)
                .and("tipoAplicaciones._id").is(idTipoAplicacion)
                .and("tipoAplicaciones.criterios._id").is(idCriterio));
		Update update = new Update().pull("tipoAplicaciones.$.criterios", new Document("_id", idCriterio));
		mongoTemplate.updateFirst(query, update, Perfil.class); 
	}
	
	

}
