package techu.practitioner.hackaton.servicio;

import techu.practitioner.hackaton.modelo.Proyecto;

public interface ServicioProyectoExt {

    void emparcharProyecto(String id, Proyecto u);
    boolean existe(String id);
}
