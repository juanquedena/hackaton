package techu.practitioner.hackaton.servicio;

import techu.practitioner.hackaton.modelo.Proyecto;

import java.util.List;

public interface ServicioGenerico<S> {

	List<S> listar();

	S obtener(String id);

	String agregar(S u);

	void actualizar(String id, S u);

	void eliminar(String id);

}
