package techu.practitioner.hackaton.servicio;


import techu.practitioner.hackaton.modelo.Perfil;


public interface ServicioPerfil extends ServicioGenerico<Perfil> {

	void emparcharPerfil(String id, Perfil p);

}
