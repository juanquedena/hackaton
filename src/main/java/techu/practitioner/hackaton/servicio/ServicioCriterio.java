package techu.practitioner.hackaton.servicio;

import java.util.List;

import techu.practitioner.hackaton.modelo.Criterio;

public interface ServicioCriterio {
	public List<Criterio> obtenerCriterios(String idPerfil, String idTp);
	
	public Criterio obtenerCriteriosId(String idPerfil, String idTp, String idCri);
	
	public String agregarCriterio(String idPerfil, String idTp, Criterio cri) ;

	public void actualizarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio, Criterio c);
	
	public void eliminarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio);
	
	public double obtenerComplejidad(String idPerfil, String idTipoAplicacion, String idCriterio, String complejidad);
}
