package techu.practitioner.hackaton.servicio.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import techu.practitioner.hackaton.modelo.Complejidad;
import techu.practitioner.hackaton.modelo.Criterio;
import techu.practitioner.hackaton.servicio.ServicioCriterio;
import techu.practitioner.hackaton.servicio.repositorio.RepositorioExtendido;
import techu.practitioner.hackaton.util.ComplejidadEnum;

@Service
public class ServicioCriterioImpl implements ServicioCriterio{

	@Autowired
	RepositorioExtendido repositorioCriterio;
	
	
	@Override
	public List<Criterio> obtenerCriterios(String idPerfil, String idTp) {
		
		return repositorioCriterio.obtenerCriterios(idPerfil, idTp);
	}


	@Override
	public Criterio obtenerCriteriosId(String idPerfil, String idTp, String idCri) {
		
		List<Criterio> criterio =  repositorioCriterio.obtenerCriteriosId(idPerfil, idTp, idCri);
		
        return criterio.isEmpty() ?  null : criterio.get(0);
	}
	
	@Override
	public String agregarCriterio(String idPerfil, String idTp, Criterio cri) {
		try {
			cri.setId(UUID.randomUUID().toString());
			repositorioCriterio.agregarCriterio(idPerfil, idTp, cri);		
			return cri.getId();
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@Override
	public void actualizarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio, Criterio c) {
		try {
			c.setId(idCriterio);
			repositorioCriterio.actualizarCriterio(idPerfil,idTipoAplicacion, idCriterio,  c);		
			
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}


	@Override
	public void eliminarCriterio(String idPerfil, String idTipoAplicacion, String idCriterio) {
		try {
	
			repositorioCriterio.eliminarCriterio(idPerfil,idTipoAplicacion, idCriterio);		
			
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}


	@Override
	public double obtenerComplejidad(String idPerfil, String idTipoAplicacion, String idCriterio, String complejidad) {
		
		Criterio criterio = this.obtenerCriteriosId(idPerfil, idTipoAplicacion, idCriterio);
		if(criterio != null ) {
			Complejidad com = criterio.getComplejidad();
			if(ComplejidadEnum.ALTA.getCodigo().equalsIgnoreCase(complejidad))
				return com.getAlto();
			
			else if(ComplejidadEnum.MEDIA.getCodigo().equalsIgnoreCase(complejidad))
				return com.getMedio();
			
			else if(ComplejidadEnum.BAJA.getCodigo().equalsIgnoreCase(complejidad))
				return com.getBajo();
		}
		return 0;
	}



}
