package techu.practitioner.hackaton.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.servicio.ServicioGenerico;
import techu.practitioner.hackaton.servicio.repositorio.RepositorioProyecto;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProyectoImpl implements ServicioGenerico<Proyecto> {

    @Autowired
    RepositorioProyecto repositorioProyecto;


    @Override
    public List<Proyecto> listar() {
        return repositorioProyecto.findAll();
    }

    @Override
    public Proyecto obtener(String id) {
        final Optional<Proyecto> p = repositorioProyecto.findById(id);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public String agregar(Proyecto u) {
        repositorioProyecto.insert(u);
        return u.getId();
    }

    @Override
    public void actualizar(String id, Proyecto u) {
        u.setId(id);
        repositorioProyecto.save(u);
    }

    @Override
    public void eliminar(String id) {
        repositorioProyecto.deleteById(id);
    }

}
