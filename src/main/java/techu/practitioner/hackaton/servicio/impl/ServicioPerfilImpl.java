package techu.practitioner.hackaton.servicio.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import techu.practitioner.hackaton.modelo.Perfil;
import techu.practitioner.hackaton.servicio.ServicioPerfil;
import techu.practitioner.hackaton.servicio.repositorio.RepositorioPerfil;

@Service
public class ServicioPerfilImpl implements ServicioPerfil {

    @Autowired
    RepositorioPerfil repositorioPerfil;

    @Autowired
    MongoTemplate mongoTemplate;

    public List<Perfil> listar() {
        return repositorioPerfil.findAll();
    }

    public Perfil obtener(String id) {
        final Optional<Perfil> p = repositorioPerfil.findById(id);
        return p.isPresent() ? p.get() : null;
    }

    public String agregar(Perfil u) {
        repositorioPerfil.insert(u);
        return u.getId();
    }

    public void actualizar(String id, Perfil u) {
        u.setId(id);
        repositorioPerfil.save(u);

    }

    public void eliminar(String id) {
        repositorioPerfil.deleteById(id);

    }

    @Override
    public void emparcharPerfil(String id, Perfil p) {
        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(id));

        final Update parche = new Update();

        if (p.getId() != null && !p.getId().trim().isEmpty())
            parche.set("id", p.getId().trim());

        if (p.getDescripcion() != null && !p.getDescripcion().trim().isEmpty())
            parche.set("descripcion", p.getDescripcion().trim());

        mongoTemplate.updateFirst(filtro, parche, p.getClass());
    }


}
