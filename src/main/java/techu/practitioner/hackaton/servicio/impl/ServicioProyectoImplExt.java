package techu.practitioner.hackaton.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.servicio.ServicioProyectoExt;
import techu.practitioner.hackaton.servicio.repositorio.RepositorioProyecto;

@Service
public class ServicioProyectoImplExt implements ServicioProyectoExt {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    RepositorioProyecto repositorioProyecto;

    public void emparcharProyecto(String id, Proyecto u) {
        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(id));

        final Update parche = new Update();

        ajustarCampo(parche, "id", u.getId());
        ajustarCampo(parche, "id_sda", u.getId_sda());
        ajustarCampo(parche, "descripcion", u.getDescripcion());

        this.mongoTemplate.updateFirst(filtro, parche, u.getClass());
    }

    private void ajustarCampo(Update u, String nombre, String valor) {
        if(valor != null && !valor.trim().isEmpty())
            u.set(nombre, valor.trim());
    }

    public boolean existe(String id) {
        if(id == null || id.trim().isEmpty()) {
            return false;
        }
        return this.repositorioProyecto.existsById(id);
    }
}
