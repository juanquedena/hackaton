package techu.practitioner.hackaton.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import techu.practitioner.hackaton.modelo.TipoAplicacion;
import techu.practitioner.hackaton.servicio.ServicioGenerico;
import techu.practitioner.hackaton.servicio.repositorio.RepositorioTipoAplicacion;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioTipoAplicacionImpl implements ServicioGenerico<TipoAplicacion> {

    @Autowired
    RepositorioTipoAplicacion repositorioTipoAplicacion;

    @Override
    public List<TipoAplicacion> listar() {
        return this.repositorioTipoAplicacion. findAll();
    }

    @Override
    public TipoAplicacion obtener(String id) {
        final Optional<TipoAplicacion> ta = this.repositorioTipoAplicacion.findById(id);
        return ta.isPresent() ? ta.get() : null;
    }

    @Override
    public String agregar(TipoAplicacion u) {
        this.repositorioTipoAplicacion.insert(u);
        return u.getId();
    }

    @Override
    public void actualizar(String id, TipoAplicacion u) {
        u.setId(id);
        this.repositorioTipoAplicacion.save(u);
    }

    @Override
    public void eliminar(String id) {
        this.repositorioTipoAplicacion.deleteById(id);
    }
}
