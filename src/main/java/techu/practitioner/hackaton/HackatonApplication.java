package techu.practitioner.hackaton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.filter.ForwardedHeaderFilter;



@SpringBootApplication
@ComponentScan(basePackages = {"com.kastkode.springsandwich.filter", "techu.practitioner.hackaton.*"})
public class HackatonApplication {
	
	
	public static void main(String[] args) {
		SpringApplication.run(HackatonApplication.class, args);
	}

	@Bean
	public ForwardedHeaderFilter forwardedHeaderFilter() {
		// X-Forwarded-Proto
		// X-Forwarded-Host
		// X-Forwarded-Port
		return new ForwardedHeaderFilter();
	}


}
