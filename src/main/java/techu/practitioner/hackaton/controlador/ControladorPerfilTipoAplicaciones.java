package techu.practitioner.hackaton.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.hackaton.modelo.Perfil;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.modelo.TipoAplicacion;
import techu.practitioner.hackaton.servicio.impl.ServicioPerfilImpl;
import techu.practitioner.hackaton.servicio.impl.ServicioTipoAplicacionImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin
@RestController
@RequestMapping(RutasApi.PERFIL_TIPO_APLICACIONES)
public class ControladorPerfilTipoAplicaciones {
    @Autowired
    private ServicioTipoAplicacionImpl servicioTipoAplicacion;

    @Autowired
    private ServicioPerfilImpl servicioPerfil;

    @GetMapping
    public CollectionModel<EntityModel<TipoAplicacionResumen>> obtenerTipoAplicacionResumen(@PathVariable(name = "id_perfil") String id) {
        final Perfil p = buscarPerfil(id);

        if(p.getTipoAplicaciones() == null )
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        final List<TipoAplicacion> tiposaplicacion = p.getTipoAplicaciones();
        return CollectionModel.of(tiposaplicacion.stream().map(
                ta -> EntityModel
                        .of(new TipoAplicacionResumen(ta)).add(crearEnlaces(id, ta))
        ).collect(Collectors.toUnmodifiableList()));
    }

    @GetMapping("/{id_tipo_aplicacion}")
    public EntityModel<TipoAplicacionResumen> obtenerTipoAplicacion(
            @PathVariable(name = "id_perfil") String id_perfil,
            @PathVariable(name = "id_tipo_aplicacion") String id_tipo_aplicacion) {

        final Perfil p = buscarPerfil(id_perfil);
        boolean encontrado = false;
        int j = 0;
        for(int i = 0; i < p.getTipoAplicaciones().size(); i++) {
            if(p.getTipoAplicaciones().get(i).getId().equalsIgnoreCase(id_tipo_aplicacion)) {
                j = i;
                encontrado = true;
            }
        }
        if(encontrado) {
            TipoAplicacion ta = p.getTipoAplicaciones().get(j);
            return EntityModel.of(new ControladorPerfilTipoAplicaciones.TipoAplicacionResumen(ta)).add(ControladorPerfilTipoAplicaciones.crearEnlaces(id_perfil, ta));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity agregarTipoAplicacionPerfil(@PathVariable(name = "id_perfil") String id, @RequestBody TipoAplicacion tipoAplicacionModel) {

        if(tipoAplicacionModel.getDescripcion() == null || tipoAplicacionModel.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        final Perfil p = this.buscarPerfil(id);
        TipoAplicacion ta = new TipoAplicacion();
        ta.setId(UUID.randomUUID().toString());
        ta.setDescripcion(tipoAplicacionModel.getDescripcion());
        ta.setCriterios(tipoAplicacionModel.getCriterios());
        if(p.getTipoAplicaciones() == null) {
            List<TipoAplicacion> ListTA = new ArrayList<>();
            ListTA.add(ta);
            p.setTipoAplicaciones(ListTA);
        }
        else{
            p.getTipoAplicaciones().add(ta);
        }
        servicioPerfil.actualizar(id,p);
        return ResponseEntity
                .created(obtenerUrl(id, ta.getId()).toUri())
                .build();
    }

    @DeleteMapping("/{id_tipo_aplicacion}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarReferenciaTipoAplicacion(@PathVariable(name = "id_perfil") String id_perfil,
                                              @PathVariable(name = "id_tipo_aplicacion") String id_tipo_aplicacion) {
        final Perfil p = buscarPerfil(id_perfil);
        boolean encontrado = false;
        int j = 0;
        for(int i = 0; i < p.getTipoAplicaciones().size(); i++) {
            if(p.getTipoAplicaciones().get(i).getId().equalsIgnoreCase(id_tipo_aplicacion)) {
                j = i;
                encontrado = true;
            }
        }
        if(encontrado) {
            p.getTipoAplicaciones().remove(j);
            servicioPerfil.actualizar(id_perfil, p);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    public static Link obtenerUrl(String id_perfil, String id_tipo_aplicacion){
        return linkTo(methodOn(ControladorPerfilTipoAplicaciones.class).obtenerTipoAplicacion(id_perfil, id_tipo_aplicacion))
                .withSelfRel()
                .withTitle("Ver detalles del Tipo de Aplicación");
    }

    public static final List<Link> crearEnlaces(String id, TipoAplicacion ta) {
        return Arrays.asList(
                linkTo(methodOn(ControladorPerfilTipoAplicaciones.class)
                        .obtenerTipoAplicacion(id, ta.getId())).withSelfRel()
        );
    }

    public Perfil buscarPerfil(String id_perfil){
        Perfil p = servicioPerfil.obtener(id_perfil);
        if (p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    public static class TipoAplicacionResumen {

        private String descripcion;

        public TipoAplicacionResumen(){}

        public TipoAplicacionResumen(TipoAplicacion ta) {
            this.descripcion = ta.getDescripcion();
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }
    }
}
