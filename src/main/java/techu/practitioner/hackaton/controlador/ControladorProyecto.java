package techu.practitioner.hackaton.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.servicio.ServicioProyectoExt;
import techu.practitioner.hackaton.servicio.impl.ServicioProyectoImpl;

import java.util.Arrays;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RequestMapping(RutasApi.PROYECTO)
@RestController
public class ControladorProyecto {

    @Autowired
    ServicioProyectoImpl servicioProyecto;

    @Autowired
    ServicioProyectoExt servicioProyectoExt;

    @GetMapping
    public EntityModel<Proyecto> listarProyectoId(@PathVariable(name="id_proyecto") String id) {
        final Proyecto pr = servicioProyecto.obtener(id);
        if (pr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return EntityModel.of(pr).add(ControladorProyectos.crearEnlaces(pr));
    }


    @PutMapping
    public EntityModel<Proyecto> actualizarProyecto(@PathVariable String id_proyecto,
                                             @RequestBody Proyecto proyectoModel) {
        Proyecto pr = servicioProyecto.obtener(id_proyecto);
        if (pr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(proyectoModel.getDescripcion() == null || proyectoModel.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(proyectoModel.getId_sda() == null || proyectoModel.getId_sda().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        pr.setDescripcion(proyectoModel.getDescripcion());
        pr.setId_sda(proyectoModel.getId_sda());

        this.servicioProyecto.actualizar(id_proyecto, proyectoModel);
        return EntityModel.of(pr).add(ControladorProyectos.crearEnlaces(pr));
    }


    @DeleteMapping
    public ResponseEntity eliminarProyecto(@PathVariable String id_proyecto) {
        Proyecto pr = servicioProyecto.obtener(id_proyecto);
        if(pr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        this.servicioProyecto.eliminar(id_proyecto);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping
    public EntityModel<Proyecto> modificarProyecto(@PathVariable String id_proyecto,
                                                   @RequestBody Proyecto proyectoModel){
        Proyecto pr = servicioProyecto.obtener(id_proyecto);
        if (pr == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(proyectoModel.getDescripcion() != null)
            if(proyectoModel.getDescripcion().trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            else
                pr.setDescripcion(proyectoModel.getDescripcion());

        if(proyectoModel.getId_sda() != null)
            if(proyectoModel.getId_sda().trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            else
                pr.setId_sda(proyectoModel.getId_sda());

        this.servicioProyectoExt.emparcharProyecto(id_proyecto, pr);
        return EntityModel.of(pr).add(ControladorProyectos.crearEnlaces(pr));
    }
}
