package techu.practitioner.hackaton.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import techu.practitioner.hackaton.modelo.Actividad;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.modelo.Requerimiento;
import techu.practitioner.hackaton.servicio.ServicioCriterio;
import techu.practitioner.hackaton.servicio.impl.ServicioProyectoImpl;


@RequestMapping(RutasApi.CALCULADORA)
@RestController
public class ControladorCalculadora {

    @Autowired
    ServicioProyectoImpl servicioProyecto;

    @Autowired
    ServicioCriterio servicioCriterio;

    @GetMapping
    public ResponseEntity<String> estimacion(@PathVariable(name = "id_proyecto") String idProyecto, @RequestParam(required = true) int diasHabiles,
                                             @RequestParam(required = true) int horasXdia) {
        if (diasHabiles <= 0 || horasXdia <= 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Proyecto p = servicioProyecto.obtener(idProyecto);

        if (p.getRequerimientos().isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        final List<Requerimiento> requerimientos = p.getRequerimientos();
        Double pfTotal = 0.0;
        for (Requerimiento r : requerimientos) {
            for (Actividad a : r.getActividades()) {
                pfTotal = pfTotal + (a.getCantidad() * servicioCriterio.obtenerComplejidad(a.getId_perfil(), a.getId_tipo_aplicacion(), a.getId_criterio(), a.getComplejidad()));
            }
        }
        Double estimacionDias = pfTotal / horasXdia;
        Double estimacionMes = estimacionDias / diasHabiles;
        Double sprint = estimacionDias / 15;
        Double fte = (sprint * 0.17) / 1;

        return new ResponseEntity<String>(
                "Estimación en días: " + String.format("%.2f", estimacionDias) +
                        " Estimación en meses: " + String.format("%.2f", estimacionMes) +
                        " Estimación por sprint: " + String.format("%.2f", sprint) +
                        " Estimación por fte: " + String.format("%.2f", fte), HttpStatus.OK);
    }
}
