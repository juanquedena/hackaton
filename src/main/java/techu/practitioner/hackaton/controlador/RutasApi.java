package techu.practitioner.hackaton.controlador;

public class RutasApi {

    public static final String API_VERSION = "/softwareEstimate/v1";
    public static final String AUTHENTICATE                         = API_VERSION + "/authenticate";
    public static final String PERFILES                             = API_VERSION + "/perfiles";
    public static final String PERFIL_TIPO_APLICACIONES             = API_VERSION + "/perfiles/{id_perfil}/tipos_aplicaciones";
    public static final String PERFIL_TIPO_APLICACION_CRITERIOS     = API_VERSION + "/perfiles/{id_perfil}/tipos_aplicaciones/{id_tipo_aplicacion}/criterios";
    public static final String PROYECTOS                            = API_VERSION + "/proyectos";
    public static final String PROYECTO                             = API_VERSION + "/proyectos/{id_proyecto}";
    public static final String PROYECTO_REQUERIMIENTOS              = API_VERSION + "/proyectos/{id_proyecto}/requerimientos";
    public static final String PROYECTO_REQUERIMIENTO_ACTIVIDADES   = API_VERSION + "/proyectos/{id_proyecto}/requerimientos/{id_requerimiento}/actividades";
    public static final String CALCULADORA  						= API_VERSION + "/calculadora/{id_proyecto}";
}
