package techu.practitioner.hackaton.controlador;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.hackaton.modelo.Perfil;
import techu.practitioner.hackaton.modelo.Perfil;
import techu.practitioner.hackaton.servicio.ServicioPerfil;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin
@RestController
@RequestMapping(RutasApi.PERFILES)
public class ControladorPerfil {

    @Autowired
    private ServicioPerfil servicioPerfil;

    @GetMapping
    public CollectionModel<EntityModel<Perfil>> listarPerfil() {
        final List<Perfil> perfiles = servicioPerfil.listar();

        return CollectionModel.of(perfiles.stream().map(p -> listarPerfiles(p)).collect(Collectors.toUnmodifiableList()));
    }

    @GetMapping("/{idPerfil}")
    public EntityModel<Perfil> obtenerPerfil(@PathVariable(name = "idPerfil") String id) {
        final Perfil p = buscarPerfilPorId(id);
        return listarPerfiles(p);
    }

    @PostMapping
    public ResponseEntity<Perfil> agregarPerfil(@RequestBody Perfil perfil) {
        if (perfil.getDescripcion() == null || perfil.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        servicioPerfil.agregar(perfil);

        return ResponseEntity
                .ok()
                .location(obtenerLinkPerfil(perfil.getId()).toUri())
                .build();
    }

    @PutMapping("/{idPerfil}")
    public EntityModel<Perfil> actualizarPerfil(@PathVariable(name = "idPerfil") String id, @RequestBody Perfil perfil) {
        Perfil p = buscarPerfilPorId(id);

        if (perfil.getDescripcion() == null || perfil.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        p.setDescripcion(perfil.getDescripcion());
        servicioPerfil.actualizar(id, perfil);
        return listarPerfiles(p);
    }
    
    @PatchMapping
    public EntityModel<Perfil> modificarPerfil(@PathVariable(name = "idPerfil") String id,
                                                   @RequestBody Perfil perfil){
        Perfil p = buscarPerfilPorId(id);

        if (perfil.getDescripcion() == null || perfil.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        p.setDescripcion(perfil.getDescripcion());
        servicioPerfil.emparcharPerfil(id, p);
        return listarPerfiles(p);
    }

    @DeleteMapping("/{idPerfil}")
    public ResponseEntity eliminarPerfil(@PathVariable(name = "idPerfil") String id) {
        Perfil p = buscarPerfilPorId(id);
        servicioPerfil.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private EntityModel<Perfil> listarPerfiles(Perfil p) {
        return EntityModel.of(p)
                .add(Arrays.asList(
                        obtenerLinkPerfil(p.getId()),
                        linkTo(methodOn(getClass()).listarPerfil()).withRel("Perfiles").withTitle("Todos los Perfiles")));
    }

    private Link obtenerLinkPerfil(String id) {
        return linkTo(methodOn(getClass()).obtenerPerfil(id))
                .withSelfRel();
    }

    private Perfil buscarPerfilPorId(String idPerfil) {
        final Perfil p = servicioPerfil.obtener(idPerfil);
        if (p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
