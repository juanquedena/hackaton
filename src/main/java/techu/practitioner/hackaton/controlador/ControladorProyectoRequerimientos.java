package techu.practitioner.hackaton.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.modelo.Requerimiento;
import techu.practitioner.hackaton.servicio.impl.ServicioProyectoImpl;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasApi.PROYECTO_REQUERIMIENTOS)
public class ControladorProyectoRequerimientos {

    @Autowired
    ServicioProyectoImpl servicioProyecto;

    @GetMapping
    public CollectionModel<EntityModel<RequerimientoResumen>> obtenerRequerimientosProyecto(@PathVariable(name = "id_proyecto") String id) {
        final Proyecto p = buscarProyectoPorId(id);

        if (p.getRequerimientos().isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        final List<Requerimiento> requerimientos = p.getRequerimientos();

        return CollectionModel.of(requerimientos.stream().map(
                r -> EntityModel
                        .of(new RequerimientoResumen(r)).add(crearEnlaces(id, r))
        ).collect(Collectors.toUnmodifiableList())).add(ControladorProyectos.crearEnlaces(p));
    }

    @GetMapping("/{id_requerimiento}")
    public EntityModel<RequerimientoResumen> obtenerRequerimientoProyecto(
            @PathVariable(name = "id_proyecto") String id,
            @PathVariable(name = "id_requerimiento") String id_requerimiento) {
        final Proyecto x = buscarProyectoPorId(id);
        boolean encontrado = false;
        int j = 0;
        for (int i = 0; i < x.getRequerimientos().size(); i++) {
            if (x.getRequerimientos().get(i).getId().equalsIgnoreCase(id_requerimiento)) {
                j = i;
                encontrado = true;
            }
        }
        if (encontrado) {
            Requerimiento r = x.getRequerimientos().get(j);
            return EntityModel.of(new RequerimientoResumen(r)).add(crearEnlaces(id, r));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Requerimiento> agregarReferenciaRequerimiento(@PathVariable(name = "id_proyecto") String id,
                                                                        @RequestBody RequerimientoResumen p) {
        if (p.getDescripcion() == null || p.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        final Proyecto x = buscarProyectoPorId(id);

        Requerimiento r = new Requerimiento();
        r.setId(UUID.randomUUID().toString());
        r.setDescripcion(p.getDescripcion());
        x.getRequerimientos().add(r);
        servicioProyecto.actualizar(id, x);

        return ResponseEntity
                .created(obtenerUrl(id, r.getId()).toUri())
                .build();
    }

    @PutMapping("{id_requerimiento}")
    public EntityModel<RequerimientoResumen> modificarReferenciaRequerimiento(@PathVariable(name = "id_proyecto") String id,
                                                                              @PathVariable String id_requerimiento,
                                                                              @RequestBody RequerimientoResumen p) {
        if (p.getDescripcion() == null || p.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        final Proyecto x = buscarProyectoPorId(id);
        boolean encontrado = false;
        int j = 0;
        for (int i = 0; i < x.getRequerimientos().size(); i++) {
            if (x.getRequerimientos().get(i).getId().equalsIgnoreCase(id_requerimiento)) {
                j = i;
                encontrado = true;
            }
        }
        if (encontrado) {
            Requerimiento r = x.getRequerimientos().get(j);
            r.setDescripcion(p.getDescripcion());
            servicioProyecto.actualizar(id, x);
            return EntityModel.of(new RequerimientoResumen(r)).add(crearEnlaces(id, r));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id_requerimiento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarReferenciaRequerimiento(@PathVariable(name = "id_proyecto") String id,
                                              @PathVariable(name = "id_requerimiento") String id_requerimiento) {
        final Proyecto x = buscarProyectoPorId(id);
        boolean encontrado = false;
        int j = 0;
        for (int i = 0; i < x.getRequerimientos().size(); i++) {
            if (x.getRequerimientos().get(i).getId().equalsIgnoreCase(id_requerimiento)) {
                j = i;
                encontrado = true;
            }
        }
        if (encontrado) {
            x.getRequerimientos().remove(j);
            servicioProyecto.actualizar(id, x);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private static Link obtenerUrl(String idProyecto, String idReq) {
        return linkTo(methodOn(ControladorProyectoRequerimientos.class).obtenerRequerimientoProyecto(idProyecto, idReq))
                .withSelfRel()
                .withTitle("Ver detalles de este requerimiento");
    }

    private Proyecto buscarProyectoPorId(String id) {
        final Proyecto p = servicioProyecto.obtener(id);
        if (p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

    public static final List<Link> crearEnlaces(String id, Requerimiento r) {
        return Arrays.asList(
                linkTo(methodOn(ControladorProyectoRequerimientos.class)
                        .obtenerRequerimientoProyecto(id, r.getId())).withSelfRel()
        );
    }

    public static class RequerimientoResumen {

        private String descripcion;

        public RequerimientoResumen() {
        }

        public RequerimientoResumen(Requerimiento r) {
            this.descripcion = r.getDescripcion();
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }
    }

}
