package techu.practitioner.hackaton.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.hackaton.auth.JwtBuilder;

@RestController
@RequestMapping(RutasApi.AUTHENTICATE)
public class ControladorAutenticacion {

    @Autowired
    JwtBuilder jwtBuilder;

    @PostMapping
    public String generarToken(@RequestBody Auth auth){
        if(!"techU".equals(auth.userId)
                || !"p4ssw0rd+#".equals(auth.passwd)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        return jwtBuilder.generateToken(auth.userId,"admin");
    }

    public static class Auth {
        public String userId;
        public String passwd;
    }
}
