package techu.practitioner.hackaton.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.modelo.Actividad;
import techu.practitioner.hackaton.modelo.Requerimiento;
import techu.practitioner.hackaton.servicio.impl.ServicioProyectoImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasApi.PROYECTO_REQUERIMIENTO_ACTIVIDADES)
public class ControladorProyectoRequerimientoActividades {

    @Autowired
    ServicioProyectoImpl servicioProyecto;

    @GetMapping
    public CollectionModel<EntityModel<ActividadResumen>> obtenerActividadesRequerimientoProyecto(
            @PathVariable(name = "id_proyecto") String id,
            @PathVariable String id_requerimiento) {
        final Proyecto p = buscarProyectoPorId(id);
        final Requerimiento r = buscarActividadPorRequerimiento(p, id_requerimiento);

        if (r.getActividades() == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        final List<Actividad> actividades = r.getActividades();

        return CollectionModel.of(actividades.stream().map(
                rx -> EntityModel
                        .of(new ActividadResumen(rx)).add(crearEnlaces(id, id_requerimiento, rx))
        ).collect(Collectors.toUnmodifiableList())).add(crearEnlaces(id, id_requerimiento));
    }

    @GetMapping("/{id_actividad}")
    public EntityModel<ActividadResumen> obtenerActividadProyecto(
            @PathVariable(name = "id_proyecto") String id,
            @PathVariable String id_requerimiento,
            @PathVariable String id_actividad) {
        final Proyecto p = buscarProyectoPorId(id);
        final Requerimiento r = buscarActividadPorRequerimiento(p, id_requerimiento);
        final List<Actividad> actividades = r.getActividades();

        boolean encontrado = false;
        int j = 0;
        for (int i = 0; i < actividades.size(); i++) {
            if (actividades.get(i).getId().equalsIgnoreCase(id_actividad)) {
                j = i;
                encontrado = true;
            }
        }
        if (encontrado) {
            Actividad rx = actividades.get(j);
            return EntityModel.of(new ActividadResumen(rx)).add(crearEnlaces(id, id_requerimiento, rx));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id_actividad}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarReferenciaActividad(@PathVariable(name = "id_proyecto") String id,
                                          @PathVariable String id_requerimiento,
                                          @PathVariable String id_actividad) {
        final Proyecto p = buscarProyectoPorId(id);
        final Requerimiento r = buscarActividadPorRequerimiento(p, id_requerimiento);
        final List<Actividad> actividades = r.getActividades();

        boolean encontrado = false;
        int j = 0;
        for (int i = 0; i < actividades.size(); i++) {
            if (actividades.get(i).getId().equalsIgnoreCase(id_actividad)) {
                j = i;
                encontrado = true;
            }
        }
        if (encontrado) {
            actividades.remove(j);
            servicioProyecto.actualizar(id, p);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Actividad> agregarReferenciaActividad(@PathVariable(name = "id_proyecto") String id,
                                                                @PathVariable String id_requerimiento,
                                                                @RequestBody ActividadResumen ar) {
        if (ar.getDescripcion() == null || ar.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (ar.getId_perfil() == null || ar.getId_perfil().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (ar.getId_tipo_aplicacion() == null || ar.getId_tipo_aplicacion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (ar.getId_criterio() == null || ar.getId_criterio().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (ar.getComplejidad() == null || ar.getComplejidad().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (ar.getCantidad() == null || ar.getCantidad() < 1)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        final Proyecto p = buscarProyectoPorId(id);
        final Requerimiento r = buscarActividadPorRequerimiento(p, id_requerimiento);

        Actividad rx = new Actividad();
        rx.setId(UUID.randomUUID().toString());
        rx.setDescripcion(ar.getDescripcion());
        rx.setId_perfil(ar.getId_perfil());
        rx.setId_tipo_aplicacion(ar.getId_tipo_aplicacion());
        rx.setId_criterio(ar.getId_criterio());
        rx.setComplejidad(ar.getComplejidad());
        rx.setCantidad(ar.getCantidad());
        r.getActividades().add(rx);
        servicioProyecto.actualizar(id, p);

        return ResponseEntity
                .created(obtenerUrl(id, id_requerimiento, rx.getId()).toUri())
                .build();
    }

    @PatchMapping("{id_actividad}")
    public EntityModel<ActividadResumen> modificarReferenciaActividad(@PathVariable(name = "id_proyecto") String id,
                                                                      @PathVariable String id_requerimiento,
                                                                      @PathVariable String id_actividad,
                                                                      @RequestBody ActividadResumen ar) {
        final Proyecto p = buscarProyectoPorId(id);
        final Requerimiento r = buscarActividadPorRequerimiento(p, id_requerimiento);
        final List<Actividad> actividades = r.getActividades();
        List<Actividad> actividadesTmp = new ArrayList<>();

        boolean encontrado = false;
        int j = 0;
        for (int i = 0; i < actividades.size(); i++) {
            if (actividades.get(i).getId().equalsIgnoreCase(id_actividad)) {
                j = i;
                encontrado = true;
            } else {
                actividadesTmp.add(actividades.get(i));
            }
        }
        if (encontrado) {
            Actividad rx = actividades.get(j);

            if (ar.getDescripcion() != null)
                if (ar.getDescripcion().trim().isEmpty())
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                else
                    rx.setDescripcion(ar.getDescripcion());

            if (ar.getId_perfil() != null)
                if (ar.getId_perfil().trim().isEmpty())
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                else
                    rx.setId_perfil(ar.getId_perfil());

            if (ar.getId_tipo_aplicacion() != null)
                if (ar.getId_tipo_aplicacion().trim().isEmpty())
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                else
                    rx.setId_tipo_aplicacion(ar.getId_tipo_aplicacion());

            if (ar.getId_criterio() != null)
                if (ar.getId_criterio().trim().isEmpty())
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                else
                    rx.setId_criterio(ar.getId_criterio());

            if (ar.getComplejidad() != null)
                if (ar.getComplejidad().trim().isEmpty())
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                else
                    rx.setComplejidad(ar.getComplejidad());

            if (ar.getCantidad() != null)
                if (ar.getCantidad() < 1)
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                else
                    rx.setCantidad(ar.getCantidad());

            actividadesTmp.add(rx);
            r.setActividades(actividadesTmp);
            servicioProyecto.actualizar(id, p);
            return EntityModel.of(new ActividadResumen(rx)).add(crearEnlaces(id, id_requerimiento, rx));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    private static Link obtenerUrl(String idProyecto, String idReq, String idActividad) {
        return linkTo(methodOn(ControladorProyectoRequerimientoActividades.class).obtenerActividadProyecto(idProyecto, idReq, idActividad))
                .withSelfRel()
                .withTitle("Ver detalles de esta actividad");
    }

    private Proyecto buscarProyectoPorId(String id) {
        final Proyecto p = servicioProyecto.obtener(id);
        if (p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return p;
    }

    private Requerimiento buscarActividadPorRequerimiento(Proyecto p, String id_requerimiento) {
        boolean encontrado = false;
        int j = 0;
        for (int i = 0; i < p.getRequerimientos().size(); i++) {
            if (p.getRequerimientos().get(i).getId().equalsIgnoreCase(id_requerimiento)) {
                j = i;
                encontrado = true;
            }
        }
        if (encontrado) {
            return p.getRequerimientos().get(j);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    public static final List<Link> crearEnlaces(String id, String id_requerimiento, Actividad r) {
        return Arrays.asList(
                linkTo(methodOn(ControladorProyectoRequerimientoActividades.class)
                        .obtenerActividadProyecto(id, id_requerimiento, r.getId())).withSelfRel()
        );
    }

    public static final List<Link> crearEnlaces(String id, String id_requerimiento) {
        Proyecto p = new Proyecto();
        p.setId(id);
        List<Link> enlaces = new ArrayList<>(ControladorProyectos.crearEnlaces(p));
        enlaces.add(linkTo(methodOn(ControladorProyectoRequerimientos.class)
                .obtenerRequerimientoProyecto(id, id_requerimiento)).withRel("requerimiento"));
        return enlaces;
    }


    public static class ActividadResumen {

        private String descripcion;
        private String id_perfil;
        private String id_tipo_aplicacion;
        private String id_criterio;
        private String complejidad;
        private Integer cantidad;

        public ActividadResumen() {
        }

        public ActividadResumen(Actividad r) {
            this.descripcion = r.getDescripcion();
            this.id_perfil = r.getId_perfil();
            this.id_tipo_aplicacion = r.getId_tipo_aplicacion();
            this.id_criterio = r.getId_criterio();
            this.complejidad = r.getComplejidad();
            this.cantidad = r.getCantidad();
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getId_perfil() {
            return id_perfil;
        }

        public void setId_perfil(String id_perfil) {
            this.id_perfil = id_perfil;
        }

        public String getId_tipo_aplicacion() {
            return id_tipo_aplicacion;
        }

        public void setId_tipo_aplicacion(String id_tipo_aplicacion) {
            this.id_tipo_aplicacion = id_tipo_aplicacion;
        }

        public String getId_criterio() {
            return id_criterio;
        }

        public void setId_criterio(String id_criterio) {
            this.id_criterio = id_criterio;
        }

        public String getComplejidad() {
            return complejidad;
        }

        public void setComplejidad(String complejidad) {
            this.complejidad = complejidad;
        }

        public Integer getCantidad() {
            return cantidad;
        }

        public void setCantidad(Integer cantidad) {
            this.cantidad = cantidad;
        }
    }

}
