package techu.practitioner.hackaton.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import techu.practitioner.hackaton.modelo.Proyecto;
import techu.practitioner.hackaton.servicio.impl.ServicioProyectoImpl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RequestMapping(RutasApi.PROYECTOS)
@RestController
public class ControladorProyectos {

    @Autowired
    ServicioProyectoImpl servicioProyecto;

    @GetMapping
    public CollectionModel<EntityModel<Proyecto>> listarProyecto()
    {
        final List<Proyecto> proyectos = this.servicioProyecto.listar();
        return CollectionModel
                .of(proyectos
                        .stream()
                        .map(p -> EntityModel.of(p).add(crearEnlaces(p)))
                        .collect(Collectors.toUnmodifiableList()));
    }

    @PostMapping
    public ResponseEntity agregarProyecto(@RequestBody Proyecto proyectoModel) {
        if(proyectoModel.getDescripcion() == null || proyectoModel.getDescripcion().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(proyectoModel.getId_sda() == null || proyectoModel.getId_sda().trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        servicioProyecto.agregar(proyectoModel);

        return ResponseEntity
                .created(obtenerUrl(proyectoModel).toUri())
                .build();
    }

    private static Link obtenerUrl(Proyecto proyectoModel){
        return linkTo(methodOn(ControladorProyecto.class).listarProyectoId(proyectoModel.getId()))
                .withSelfRel()
                .withTitle("Ver detalles de este proyecto");
    }

    public static List<Link> crearEnlaces(Proyecto p) {
        return Arrays.asList(
                obtenerUrl(p),

                linkTo(methodOn(ControladorProyectoRequerimientos.class).obtenerRequerimientosProyecto(p.getId()))
                        .withRel("requerimientos")
                        .withTitle("Ver los requerimientos de este proyecto")
        );
    }
}
