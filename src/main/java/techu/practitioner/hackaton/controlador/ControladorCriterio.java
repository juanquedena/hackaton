package techu.practitioner.hackaton.controlador;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import techu.practitioner.hackaton.modelo.Criterio;
import techu.practitioner.hackaton.modelo.Perfil;
import techu.practitioner.hackaton.modelo.TipoAplicacion;
import techu.practitioner.hackaton.servicio.ServicioCriterio;
import techu.practitioner.hackaton.servicio.ServicioGenerico;


@RestController
@RequestMapping(RutasApi.PERFIL_TIPO_APLICACION_CRITERIOS)
public class ControladorCriterio {

    @Autowired
    ServicioCriterio servicioCriterio;

    @Autowired
    ServicioGenerico<Perfil> servicioPerfil;

    @Autowired
    ServicioGenerico<TipoAplicacion> servicioTipoAplicacion;


    @GetMapping
    public CollectionModel<EntityModel<Criterio>> obtenerCriterios(@PathVariable(name = "id_perfil") String idPerfil, @PathVariable(name = "id_tipo_aplicacion") String idTipoAplicacion) {

        Perfil p = servicioPerfil.obtener(idPerfil);
        if (p == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Perfil no existe");
        }
//		TipoAplicacion tp = servicioTipoAplicacion.obtener(idTipoAplicacion);
//		if (tp == null) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Tipo de Aplicación no existe");
//        }
        List<Criterio> criterios = servicioCriterio.obtenerCriterios(idPerfil, idTipoAplicacion);

        if (criterios == null || criterios.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return CollectionModel.of(criterios.stream().map(c -> EntityModel.of(c).add(
                        linkTo(methodOn(this.getClass()).obtenerCriterioId(idPerfil, idTipoAplicacion, c.getId()))
                                .withSelfRel()
                                .withTitle("Ver detalles de criterio")))
                .collect(Collectors.toUnmodifiableList()));

    }


    @GetMapping("{id_criterio}")
    public EntityModel<Criterio> obtenerCriterioId(@PathVariable(name = "id_perfil") String idPerfil, @PathVariable(name = "id_tipo_aplicacion") String idTipoAplicacion, @PathVariable(name = "id_criterio") String idCriterio) {

        Criterio criterio = servicioCriterio.obtenerCriteriosId(idPerfil, idTipoAplicacion, idCriterio);
        if (criterio == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro criterio");
        }
        return EntityModel.of(criterio).add(
                linkTo(methodOn(this.getClass()).obtenerCriterioId(idPerfil, idTipoAplicacion, idCriterio)).withSelfRel());

    }

    @PostMapping
    public ResponseEntity<?> agregarCriterio(@PathVariable(name = "id_perfil") String idPerfil, @PathVariable(name = "id_tipo_aplicacion") String idTipoAplicacion, @RequestBody Criterio c) {

        servicioCriterio.agregarCriterio(idPerfil, idTipoAplicacion, c);
        return ResponseEntity
                .ok()
                .location(
                        linkTo(methodOn(this.getClass()).obtenerCriterioId(idPerfil, idTipoAplicacion, c.getId())).withSelfRel().toUri())
                .build();
    }

    @PutMapping("{id_criterio}")
    public void actualizarCriterio(@PathVariable(name = "id_perfil") String idPerfil, @PathVariable(name = "id_tipo_aplicacion") String idTipoAplicacion,
                                   @PathVariable(name = "id_criterio") String idCriterio, @RequestBody Criterio c) {

        servicioCriterio.actualizarCriterio(idPerfil, idTipoAplicacion, idCriterio, c);

    }

    @DeleteMapping("{id_criterio}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarCriterio(@PathVariable(name = "id_perfil") String idPerfil, @PathVariable(name = "id_tipo_aplicacion") String idTipoAplicacion,
                                 @PathVariable(name = "id_criterio") String idCriterio) {

        servicioCriterio.eliminarCriterio(idPerfil, idTipoAplicacion, idCriterio);

    }


}
