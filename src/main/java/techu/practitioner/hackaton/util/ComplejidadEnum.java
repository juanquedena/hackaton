package techu.practitioner.hackaton.util;

public enum ComplejidadEnum {

    MEDIA("media", "Complejidad media"), BAJA("baja", "Complejidad media"), ALTA("alta", "Complejidad alta");

    private String codigo;
    private String descripcion;

    private ComplejidadEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
